### statusd

Simple daemon which gather different type of status information from system, then expose those information through fifo files under XDG_RUNTIME_DIR/statusd directory. This fifo files can be used to get data out of this daemon.

Main reason to write this tool is to minimize pid utilization when using i3blocks and similar apps.

#### Dependencies

##### Runtime

* libpipewire-0.3

##### Build

* golang
* gcc
* meson
* ninja
* make
* libpipewire-0.3

#### Installation

* clone this repo
* under this repo directory, run the following command
  ```
  $ make
  $ DESTDIR="${HOME}"/statusd make install
  ```
* above commands will create statusd directory under your HOME directory. There you will see `statusd/bin/statusd` binary, run this binary, thats all.

