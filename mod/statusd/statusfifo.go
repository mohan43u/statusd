package statusd

import (
	"fmt"
	"os"
	"strings"
	"syscall"
)

type StatusFifo struct {
	Fifo *os.File
}

func (self *StatusFifo) Initialize(name string) bool {
	var fifo *os.File
	fifo, err := os.OpenFile(name, os.O_RDWR, 0)
	if err != nil {
		if strings.Contains(err.Error(), "no such file or directory") {
			err = syscall.Mkfifo(name, 0777)
			if err != nil {
				fmt.Println("failed to create fifo:", err)
				return false
			}
			fifo, err = os.OpenFile(name, os.O_RDWR, 0)
			if err != nil {
				fmt.Println("failed to open fifo:", err)
				return false
			}
		} else {
			fmt.Println("failed to open fifo:", err)
			return false
		}
	}
	if err := fifo.Chmod(0777); err != nil {
		fmt.Println("failed to change permission of fifo:", err)
		return false
	}
	self.Fifo = fifo
	return true
}

func (self *StatusFifo) Finalize() {
	if self.Fifo != nil {
		self.Fifo.Close()
		os.Remove(self.Fifo.Name())
	}
	self.Fifo = nil
}

func NewStatusFifo(name string) *StatusFifo {
	self := &StatusFifo{}
	fifoname := StatusDir()
	if len(fifoname) <= 0 {
		return nil
	}
	fifoname += name
	if self.Initialize(fifoname) == false {
		return nil
	}
	return self
}

