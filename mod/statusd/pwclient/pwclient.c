#include "pipewire/keys.h"
#include "pipewire/node.h"
#include "pipewire/pipewire.h"
#include "pipewire/context.h"
#include "pipewire/core.h"
#include "pipewire/main-loop.h"
#include "pipewire/proxy.h"
#include "pipewire/type.h"
#include "spa/debug/types.h"
#include "spa/pod/iter.h"
#include "spa/pod/pod.h"
#include "spa/utils/defs.h"
#include "spa/utils/dict.h"
#include "spa/utils/hook.h"
#include "spa/utils/list.h"
#include "spa/utils/string.h"
#include "pipewire/extensions/metadata.h"
#include "spa/debug/pod.h"
#include "spa/utils/type-info.h"
#include "spa/utils/type.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

typedef struct _RegistryInfo {
	uint32_t id;
	uint32_t permissions;
	char *type;
	uint32_t version;
	char *name;
	char *metadataname;
	struct _RegistryInfo *next;
} RegistryInfo;

typedef struct {
	struct pw_main_loop *mainloop;
	struct pw_context *context;
	struct pw_core *core;
	struct pw_registry *registry;
	struct spa_hook core_listener;
	struct spa_hook registry_listener;
	struct spa_hook metadata_listener;
	struct spa_hook node_listener;
	struct pw_metadata *metadata;
	struct pw_node *node;
	RegistryInfo *registryinfo;
	int syncseq;
	bool issource;
	char *defaultsink;
	bool defaultsinkmute;
	float defaultsinkvol[2];
	char *defaultsource;
	bool defaultsourcemute;
	float defaultsourcevol[2];
} Client;

static Client* client_new() {
	Client *client = NULL;

	client = calloc(1, sizeof(Client));
	return client;
}

static void registry_free(Client *client) {
	if(client != NULL) {
		while(client->registryinfo) {
			RegistryInfo *registryinfo = NULL;

			registryinfo = client->registryinfo;
			if (registryinfo->type != NULL) free(registryinfo->type);
			if (registryinfo->name != NULL) free(registryinfo->name);
			if (registryinfo->metadataname != NULL) free(registryinfo->metadataname);
			client->registryinfo = registryinfo->next;
			free(registryinfo);
		}
		if (client->registry != NULL) {
			spa_hook_remove(&client->registry_listener);
			pw_proxy_destroy((struct pw_proxy*) client->registry);
			client->registry = NULL;
		}
	}
}

static void client_free(Client *client) {
	if (client != NULL) {
		if (client->defaultsink != NULL) {
			free(client->defaultsink);
			client->defaultsink = NULL;
		}

		if (client->defaultsource != NULL) {
			free(client->defaultsource);
			client->defaultsource = NULL;
		}

		if(client->node != NULL) {
			spa_hook_remove(&client->node_listener);
			pw_proxy_destroy((struct pw_proxy*) client->node);
			client->node = NULL;
		}

		if (client->metadata != NULL) {
			spa_hook_remove(&client->metadata_listener);
			pw_proxy_destroy((struct pw_proxy*) client->metadata);
			client->metadata = NULL;
		}

		registry_free(client);

		if (client->core != NULL) {
			pw_core_disconnect(client->core);
			client->core = NULL;
		}

		if (client->context != NULL) {
			pw_context_destroy(client->context);
		}

		if (client->mainloop != NULL) {
			pw_main_loop_destroy(client->mainloop);
			client->mainloop = NULL;
		}

		free(client);
		client = NULL;
	}
}

static void roundtrip(Client *client) {
	client->syncseq = pw_core_sync(client->core, PW_ID_CORE, client->syncseq);
}

static void node_events_param_handler (void *data, int seq, uint32_t id, uint32_t index, uint32_t next, const struct spa_pod *param) {
	Client *client = NULL;
	struct spa_pod_prop *p = NULL;
	void *pp = NULL;
	const struct spa_type_info *bti = NULL;
	const struct spa_type_info *kti = NULL;
	const struct spa_pod_object_body *body = NULL;
	const struct spa_pod_array_body *bodyy = NULL;

	client = (Client*) data;
	if (SPA_POD_TYPE(param) != SPA_TYPE_Object) {
		dprintf(2, "param is not SPA_TYPE_object\n");
		spa_debug_pod(0, NULL, param);
		return;
	}

	body = SPA_POD_BODY(param);
	bti = spa_debug_type_find(SPA_TYPE_ROOT, body->type);
	if (bti == NULL) {
		dprintf(2, "failed to get type information of SPA_TYPE_Object\n");
		spa_debug_pod(0, NULL, param);
		return;
	}

	if (!spa_streq(bti->name, "Spa:Pod:Object:Param:Props")) {
		return;
	}

        SPA_POD_OBJECT_BODY_FOREACH(body, SPA_POD_BODY_SIZE(param), p) {
                kti = spa_debug_type_find(bti->values, p->key);
                if (kti == NULL) kti = spa_debug_type_find(SPA_TYPE_ROOT, p->key);
		if (kti == NULL) {
			dprintf(2, "not able to fetch kti\n");
			continue;
		}

		if (spa_streq(kti->name, "Spa:Pod:Object:Param:Props:mute")) {
			if(client->issource) {
				client->defaultsourcemute = (*(uint32_t*)SPA_POD_BODY(&p->value));
			}
			else {
				client->defaultsinkmute = (*(uint32_t*)SPA_POD_BODY(&p->value));
			}
		}

		if (spa_streq(kti->name, "Spa:Pod:Object:Param:Props:channelVolumes")) {
			int index = 0;
			bodyy = SPA_POD_BODY(&p->value);
			SPA_POD_ARRAY_BODY_FOREACH(bodyy, SPA_POD_BODY_SIZE(&p->value), pp) {
				if(client->issource) {
					client->defaultsourcevol[index] = (*(float*)pp);
					index++;
				}
				else {
					client->defaultsinkvol[index] = (*(float*)pp);
					index++;
				}
			}
		}
        }
}

static void node_events_info_handler (void *data, const struct pw_node_info *info) {
	Client *client = NULL;
	int index = 0;

	client = (Client*) data;
	for (index = 0; index < info->n_params; index++) {
		pw_node_enum_params(client->node, 0, info->params[index].id, 0, 0, NULL);
	}
	roundtrip(client);
}

static const struct pw_node_events node_events = {
	.version = PW_VERSION_NODE_EVENTS,
	.info = node_events_info_handler,
	.param = node_events_param_handler
};

static bool node_init(Client *client, const char *nodename) {
	struct pw_node *node = NULL;
	RegistryInfo *registryinfo = NULL;

	registryinfo = client->registryinfo;
	while(registryinfo) {
		if (registryinfo->name != NULL) {
			if (strstr(nodename, registryinfo->name)) {
				break;
			}
		}
		registryinfo = registryinfo->next;
	}
	if (registryinfo == NULL) {
		dprintf(2, "not able to find %s in registry\n", nodename);
		return false;
	}

	node = pw_registry_bind(client->registry,
				registryinfo->id,
				registryinfo->type,
				registryinfo->version,
				0);
	if (node == NULL) {
		dprintf(2, "failed to bind node\n");
		return false;
	}
	if(client->node != NULL) {
		spa_hook_remove(&client->node_listener);
		pw_proxy_destroy((struct pw_proxy*) client->node);
		client->node = NULL;
	}
	client->node = node;

	spa_zero(client->node_listener);
	pw_node_add_listener(node, &client->node_listener, &node_events, client);
	roundtrip(client);
	return true;
}

static int metadata_events_property_handler(
	void *data,
	unsigned int id,
	const char *key,
	const char *type,
	const char *value
	){
	Client *client = NULL;

	client = (Client*) data;
	if (spa_streq(key, "default.audio.sink")) {
		if (value) client->defaultsink = strdup(value);
	}
	if (spa_streq(key, "default.audio.source")) {
		if (value) client->defaultsource = strdup(value);
	}
	return 0;
}

static const struct pw_metadata_events metadata_events = {
	.version = PW_VERSION_METADATA_EVENTS,
	.property = metadata_events_property_handler
};

static bool metadata_init(Client *client) {
	RegistryInfo *registryinfo = NULL;
	struct pw_metadata *metadata = NULL;

	registryinfo = client->registryinfo;
	while(registryinfo) {
		if (spa_streq(registryinfo->type, PW_TYPE_INTERFACE_Metadata) &&
		    spa_streq(registryinfo->metadataname, "default")) {
			break;
		}
		registryinfo = registryinfo->next;
	}
	if (registryinfo == NULL) {
		dprintf(2, "not able to find default metadata in registry\n");
		return false;
	}

	metadata = pw_registry_bind(client->registry,
				    registryinfo->id,
				    registryinfo->type,
				    registryinfo->version,
				    0);
	if (metadata == NULL) {
		dprintf(2, "failed to get metadata\n");
		return false;
	}
	if (client->metadata != NULL) {
		spa_hook_remove(&client->metadata_listener);
		pw_proxy_destroy((struct pw_proxy*) client->metadata);
		client->metadata = NULL;
	}
	client->metadata = metadata;

	spa_zero(client->metadata_listener);
	pw_metadata_add_listener(client->metadata,
				 &client->metadata_listener,
				 &metadata_events,
				 client);
	roundtrip(client);
	return true;
}

static void registry_events_global_handler(
	void *data,
	uint32_t id,
	uint32_t permissions,
	const char *type,
	uint32_t version,
	const struct spa_dict *prop
	) {
	Client *client = NULL;
	RegistryInfo *registryinfo = NULL;
	const char *nodename = NULL;
	const char *metadataname = NULL;

	client = (Client*) data;

	registryinfo = calloc(1, sizeof(RegistryInfo));
	if (registryinfo == NULL) {
		dprintf(2, "failed to allocate registryinfo: %s\n", strerror(errno));
	}
	registryinfo->id = id;
	registryinfo->permissions = permissions;
	registryinfo->type = strdup(type);
	registryinfo->version = version;
        nodename = spa_dict_lookup(prop, PW_KEY_NODE_NAME);
	if (nodename != NULL) {
		registryinfo->name = strdup(nodename);
	}
	metadataname = spa_dict_lookup(prop, PW_KEY_METADATA_NAME);
	if (metadataname != NULL) {
		registryinfo->metadataname = strdup(metadataname);
	}
	if (client->registryinfo == NULL) {
		client->registryinfo = registryinfo;
	}
	else {
		registryinfo->next = client->registryinfo;
		client->registryinfo = registryinfo;
	}
}

static const struct pw_registry_events registry_events = {
	.version = PW_VERSION_REGISTRY_EVENTS,
	.global = registry_events_global_handler,
};

static void core_events_done_handler(void *data, uint32_t id, int seq) {
	Client *client = NULL;

	client = (Client*) data;
	if (id == PW_ID_CORE && seq == client->syncseq) {
		pw_main_loop_quit(client->mainloop);
	}
}

static const struct pw_core_events core_events = {
	.version = PW_VERSION_CORE_EVENTS,
	.done = core_events_done_handler
};

static bool registry_init(Client *client) {
	registry_free(client);
	client->registry = pw_core_get_registry(client->core, PW_VERSION_REGISTRY, 0);
	if (client->registry == NULL) {
		dprintf(2, "failed to get registry\n");
		return false;
	}

	spa_zero(client->registry_listener);
	pw_registry_add_listener(client->registry, &client->registry_listener, &registry_events, client);
	roundtrip(client);
	return true;
}

static void handle_signal(void *userdata, int signalno) {
	Client *client = (Client *) userdata;
	pw_main_loop_quit(client->mainloop);
}

int main(int argc, char *argv[]){
	int result = 1;
	Client *client = NULL;

	client = client_new();
	if (client == NULL) {
		dprintf(2, "failed to allocate memory: %s\n", strerror(errno));
		goto end;
	}

	pw_init(&argc, &argv);
	client->mainloop = pw_main_loop_new(NULL);
	if (client->mainloop == NULL) {
		dprintf(2, "failed to create mainloop\n");
		goto end;
	}

	client->context = pw_context_new(pw_main_loop_get_loop(client->mainloop), NULL, 0);
	if (client->context == NULL) {
		dprintf(2, "failed to get context\n");
		goto end;
	}

	client->core = pw_context_connect(client->context, NULL, 0);
	if (client->core == NULL) {
		dprintf(2, "failed to get core\n");
		goto end;
	}

	pw_loop_add_signal(pw_main_loop_get_loop(client->mainloop), SIGINT, handle_signal, client);
	pw_loop_add_signal(pw_main_loop_get_loop(client->mainloop), SIGTERM, handle_signal, client);
	spa_zero(client->core_listener);
	pw_core_add_listener(client->core, &client->core_listener, &core_events, client);

	while(true) {
		if (!registry_init(client)) {
			dprintf(2, "failed to initialize registry\n");
			goto sleep;
		}
		pw_main_loop_run(client->mainloop);

		if (!metadata_init(client)) {
			dprintf(2, "failed to initialize metadata\n");
			goto sleep;
		}
		pw_main_loop_run(client->mainloop);
	
		if (!(client->defaultsink != NULL &&
		      client->defaultsource != NULL)) {
			dprintf(2, "sink and source not found\n");
			goto sleep;
		}

		if (client->defaultsink != NULL) {
			client->issource = false;
			if (!node_init(client, client->defaultsink)) {
				dprintf(2, "sink: failed to initialize node\n");
				goto sleep;
			}
			pw_main_loop_run(client->mainloop);
		}
		if (client->defaultsource != NULL) {
			client->issource = true;
			if (!node_init(client, client->defaultsource)) {
				dprintf(2, "source: failed to initialize node\n");
				goto sleep;
			}
			pw_main_loop_run(client->mainloop);
		}

		dprintf(1, "spk: %0.2f %0.2f [mute: %d] mic: %0.2f %0.2f [mute: %d]\n",
			client->defaultsinkvol[0], client->defaultsinkvol[1], client->defaultsinkmute,
			client->defaultsourcevol[0], client->defaultsourcevol[1], client->defaultsourcemute);
	sleep:
		sleep(1);
	}
	result = 0;
end:
	client_free(client);
	return result;
}
