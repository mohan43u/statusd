package statusd

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

type StatusMemPlugin struct {
	fifo *StatusFifo
	stopRun bool
}


func (self *StatusMemPlugin) Initialize(args []string) bool {
	self.fifo = NewStatusFifo("mem")
	if self.fifo == nil {
		return false
	}
	self.stopRun = false
	return true
}

func (self *StatusMemPlugin) Run() bool {
	go func() {
		ticker := time.Tick(time.Second)
		for range ticker {
			if self.stopRun {
				break
			}
			bmem, err := os.ReadFile("/proc/meminfo")
			if err != nil {
				fmt.Println("failed to read meminfo:", err)
				continue
			}
			var memtotal float64
			var memavail float64
			var swaptotal float64
			var swapavail float64
			
			lines := strings.Split(string(bmem), "\n")
			for _, line := range lines {
				if strings.HasPrefix(line, "MemTotal:") {
					fields := strings.Fields(line)
					memtotal, err = strconv.ParseFloat(fields[1], 64)
					if err != nil {
						fmt.Println("failed to parse memtotal:", err)
						continue
					}
				}

				if strings.HasPrefix(line, "MemAvailable:") {
					fields := strings.Fields(line)
					memavail, err = strconv.ParseFloat(fields[1], 64)
					if err != nil {
						fmt.Println("failed to parse memavail:", err)
						continue
					}
				}
				
				if strings.HasPrefix(line, "SwapTotal:") {
					fields := strings.Fields(line)
					swaptotal, err = strconv.ParseFloat(fields[1], 64)
					if err != nil {
						fmt.Println("failed to parse swaptotal:", err)
						continue
					}
				}

				if strings.HasPrefix(line, "SwapAvailable:") {
					fields := strings.Fields(line)
					swapavail, err = strconv.ParseFloat(fields[1], 64)
					if err != nil {
						fmt.Println("failed to parse swapavail:", err)
						continue
					}
				}
			}
			fmt.Fprintf(self.fifo.Fifo, "memfree: %0.2f%%\n", ((memavail + swapavail) * 100)/(memtotal + swaptotal))
		}
	}()
	return true
}

func (self *StatusMemPlugin) Stop() bool {
	self.stopRun = true
	return true
}

func (self *StatusMemPlugin) Finalize() bool {
	self.fifo.Finalize()
	return true
}
