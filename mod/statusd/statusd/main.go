package main

import (
	"gitlab.com/mohan43u/statusd/mod/statusd"
)

func main() {
	plugins := map[string]statusd.StatusPlugin{}
	plugins["date"] = &statusd.StatusDatePlugin{}
	plugins["audio"] = &statusd.StatusAudioPlugin{}
	plugins["backlight"] = &statusd.StatusBacklightPlugin{}
	plugins["battery"] = &statusd.StatusBatteryPlugin{}
	plugins["mem"] = &statusd.StatusMemPlugin{}
	plugins["cpu"] = &statusd.StatusCpuPlugin{}
	plugins["gpu"] = &statusd.StatusGpuPlugin{}
	plugins["network"] = &statusd.StatusNetworkPlugin{}
	statusd.Run(plugins)
}
