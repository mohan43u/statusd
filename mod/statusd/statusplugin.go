package statusd

import (
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/adrg/xdg"
)

type StatusPlugin interface {
	Initialize(args []string) bool
	Run() bool
	Stop() bool
	Finalize() bool
}

func StatusDir() string {
	statusdir := xdg.RuntimeDir + "/statusd/"
	err := os.Mkdir(statusdir, 0755)
	if err != nil && strings.Contains(err.Error(), "file exists") == false {
		fmt.Println("failed to create runtime dir:", err)
		return ""
	}
	return statusdir
}

func Run(plugins map[string]StatusPlugin) {
	for name, plugin := range plugins{
		fmt.Println("initializing", name)
		if plugin.Initialize(os.Args) == false {
			fmt.Println("initializing", name, "failed")
		}
	}
	for name, plugin := range plugins{
		fmt.Println("starting", name)
		if plugin.Run() == false {
			fmt.Println("initializing", name, "failed")
			plugin.Stop()
			plugin.Finalize()
			delete(plugins, name)
		}
	}
	sigc := make(chan os.Signal)
	signal.Notify(sigc, syscall.SIGINT, syscall.SIGTERM)
	for {
		sig := <- sigc
		if sig == syscall.SIGINT || sig == syscall.SIGTERM {
			for name, plugin := range plugins{
				fmt.Println("stopping", name)
				if plugin.Stop() == false {
					fmt.Println("stopping", name, "failed")
					plugin.Finalize()
					delete(plugins, name)
				}
			}
			break
		}
	}
	for name, plugin := range plugins{
		fmt.Println("finalizing", name)
		if plugin.Finalize() == false {
			fmt.Println("finalizing", name, "failed")
		}
	}
}
