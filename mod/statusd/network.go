package statusd

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"

	"github.com/godbus/dbus"
)

type Address struct {
	Address [4]byte `json:"Address"`
}

type Route struct {
	Family int64 `json:"Family"`
	Destination [4]byte `json:"Destination"`
	Gateway [4]byte `json:"Gateway"`
	PreferredSource [4]byte `json:"PreferredSource"`
	ConfigState string `json:"ConfigState"`
}

type Interface struct {
	Name string `json:"Name"`
	Type string `json:"Type"`
	Driver string `json:"Driver"`
	SSID string `json:"SSID"`
	Addresses []Address `json:"Addresses"`
	Routes []Route `json:"Routes"`
}

type NetworkInfo struct {
	Interfaces []Interface `json:"Interfaces"`
}

type StatusNetworkPlugin struct {
	fifo *StatusFifo
	systembus *dbus.Conn
	networkctl dbus.BusObject
	stopRun bool
}

func (self *StatusNetworkPlugin) Initialize(args []string) bool {
	self.fifo = NewStatusFifo("network")
	if self.fifo == nil {
		return false
	}
	self.stopRun = false
	return true
}

func (self *StatusNetworkPlugin) Run() bool {
	systembus, err := dbus.SystemBus()
	if err != nil {
		fmt.Println("failed to connect to SystemBus", err)
		return false
	}
	self.systembus = systembus
	self.networkctl = self.systembus.Object("org.freedesktop.network1", "/org/freedesktop/network1")

	go func() {
		ticker := time.Tick(time.Second)
		for range ticker {
			if self.stopRun {
				break
			}

			var details string
			err = self.networkctl.Call("org.freedesktop.network1.Manager.Describe", 0).Store(&details)
			if err != nil {
				fmt.Println("failed to get network details:", err)
				self.Stop()
				break
			}

			var networkinfo NetworkInfo
			err = json.Unmarshal([]byte(details), &networkinfo)
			if err != nil {
				fmt.Println("failed to parse networkinfo:", err)
				self.Stop()
				break
			}
			
			ssids := make([]string, 0)
			ifacenames := make([]string, 0)
			addresses := make([]string, 0)

			for _, iface := range networkinfo.Interfaces {
				for _, route := range iface.Routes {
					if route.Family == 2 &&
						route.Destination == [4]byte{0, 0, 0, 0} &&
						strings.Contains(route.ConfigState, "configured") {
						ssids = append(ssids, iface.SSID)
						ifacenames = append(ifacenames, iface.Name)
						addresses = append(addresses,
							fmt.Sprintf("%d.%d.%d.%d",
								route.PreferredSource[0],
								route.PreferredSource[1],
								route.PreferredSource[2],
								route.PreferredSource[3]))
					}
				}
			}
			fmt.Fprintln(self.fifo.Fifo,
				strings.Join(ssids, " "),
				strings.Join(ifacenames, " "),
				strings.Join(addresses, " "))
		}
	}()
	return true
}

func (self *StatusNetworkPlugin) Stop() bool {
	self.stopRun = true
	self.systembus = nil
	self.networkctl = nil
	return true
}

func (self *StatusNetworkPlugin) Finalize() bool {
	self.fifo.Finalize()
	return true
}
