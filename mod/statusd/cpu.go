package statusd

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

type StatusCpuPlugin struct {
	fifo *StatusFifo
	stopRun bool
}

func (self *StatusCpuPlugin) Initialize(args []string) bool {
	self.fifo = NewStatusFifo("cpu")
	if self.fifo == nil {
		return false
	}
	self.stopRun = false
	return true
}

func (self *StatusCpuPlugin) Run() bool {
	go func() {
		var ouser float64
		var onice float64
		var osystem float64
		var oidle float64

		ticker := time.Tick(time.Second)
		for range ticker {
			if self.stopRun {
				break
			}

			bcpu, err := os.ReadFile("/proc/stat")
			if err != nil {
				fmt.Println("failed to read cpu:", err)
				continue
			}
			lines := strings.Split(string(bcpu), "\n")
			if len(lines) <= 0 {
				fmt.Println("failed to parse cpu")
				continue
			}
			line := lines[0]
			lines = nil
			fields := strings.Fields(line)

			nuser, err := strconv.ParseFloat(fields[1], 64)
			if err != nil {
				fmt.Println("failed to parse usercpu:", err)
				continue
			}
			nnice, err := strconv.ParseFloat(fields[2], 64)
			if err != nil {
				fmt.Println("failed to parse nicecpu:", err)
				continue
			}
			nsystem, err := strconv.ParseFloat(fields[3], 64)
			if err != nil {
				fmt.Println("failed to parse systemcpu:", err)
				continue
			}
			nidle, err := strconv.ParseFloat(fields[4], 64)
			if err != nil {
				fmt.Println("failed to parse idlecpu:", err)
				continue
			}
			user := nuser - ouser
			nice := nnice - onice
			system := nsystem - osystem
			idle := nidle - oidle
			ouser = nuser
			onice = nnice
			osystem = nsystem
			oidle = nidle
			fmt.Fprintf(self.fifo.Fifo, "cpu: %0.2f%%\n", ((user + nice + system) * 100)/(user + nice + system + idle))
		}
	}()
	return true
}

func (self *StatusCpuPlugin) Stop() bool {
	self.stopRun = true
	return true
}

func (self *StatusCpuPlugin) Finalize() bool {
	self.fifo.Finalize()
	return true
}
