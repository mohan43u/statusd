package statusd

import (
	"time"
)

type StatusDatePlugin struct {
	fifo *StatusFifo
	stopRun bool
}

func (self *StatusDatePlugin) Initialize(args []string) bool {
	self.fifo = NewStatusFifo("date")
	if self.fifo == nil {
		return false
	}
	self.stopRun = false
	return true
}

func (self *StatusDatePlugin) Run() bool {
	go func() {
		ticker := time.Tick(time.Second)
		for range ticker {
			if self.stopRun {
				break
			}
			timestamp := time.Now().Format("Mon 2006-01-02 15:04:05 -07:00\n")
			self.fifo.Fifo.Write([]byte(timestamp))
		}
	}()
	return true
}

func (self *StatusDatePlugin) Stop() bool {
	self.stopRun = true
	return true
}

func (self *StatusDatePlugin) Finalize() bool {
	self.fifo.Finalize()
	return true
}
