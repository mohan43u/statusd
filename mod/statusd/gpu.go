package statusd

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"strings"
)

type StatusGpuPlugin struct {
	fifo *StatusFifo
	gpu *exec.Cmd
	outreader *bufio.Scanner
	errreader *bufio.Scanner
}

func (self *StatusGpuPlugin) Initialize(args []string) bool {
	self.fifo = NewStatusFifo("gpu")
	if self.fifo == nil {
		return false
	}
	return true
}

func (self *StatusGpuPlugin) Run() bool {
	self.gpu = exec.Command("intel_gpu_top", "-l")

	outpipe, err := self.gpu.StdoutPipe()
	if err != nil {
		fmt.Println("failed to get StdoutPipe")
		return false
	}
	errpipe, err := self.gpu.StderrPipe()
	if err != nil {
		fmt.Println("failed to get StderrPipe")
		return false
	}
	self.outreader = bufio.NewScanner(outpipe)
	self.errreader = bufio.NewScanner(errpipe)

	if err := self.gpu.Start(); err != nil {
		fmt.Println("failed to start gpu command:", err)
		return false
	}
	go func() {
		err := self.gpu.Wait()
		if err != nil {
			fmt.Println("gpu exited:", err)
			self.Stop()
		}
	}()
	go func() {
		for {
			if self.outreader.Scan() {
				line := self.outreader.Text()
				fields := strings.Fields(line)
				if len(fields) <= 0 {
					fmt.Println("failed to get fields from gpu line:", line)
					break
				}
				if strings.Compare("Freq", fields[0]) == 0 ||
					strings.Compare("req", fields[0]) == 0 {
					continue
				}
				fmt.Fprintf(self.fifo.Fifo, "gpu: R=%s%% V=%s%%\n", fields[6], fields[12])
			} else {
				fmt.Println("failed to read outline:", self.outreader.Err())
				break
			}
		}
	}()
	go func() {
		for {
			if self.errreader.Scan() {
				os.Stderr.Write([]byte(self.errreader.Text()))
			} else {
				fmt.Println("failed to read errline:", self.errreader.Err())
				break
			}
		}
	}()

	return true
}

func (self *StatusGpuPlugin) Stop() bool {
	if self.gpu != nil {
		err := self.gpu.Process.Kill()
		if err != nil {
			fmt.Println("failed to kill gpu:", err)
		}
	}
	self.gpu = nil
	self.outreader = nil
	self.errreader = nil
	return true
}

func (self *StatusGpuPlugin) Finalize() bool {
	self.fifo.Finalize()
	return true
}
