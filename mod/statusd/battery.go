package statusd

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
	"github.com/gen2brain/beeep"
)

type StatusBatteryPlugin struct {
	fifo *StatusFifo
	stopRun bool
}


func (self *StatusBatteryPlugin) Initialize(args []string) bool {
	self.fifo = NewStatusFifo("battery")
	if self.fifo == nil {
		return false
	}
	self.stopRun = false
	return true
}

func (self *StatusBatteryPlugin) Run() bool {
	go func() {
		notifyinterval := 0
		ticker := time.Tick(time.Second)
		for range ticker {
			if self.stopRun {
				break
			}
			bbatterynow, err := os.ReadFile("/sys/class/hwmon/hwmon1/device/charge_now")
			if err != nil {
				fmt.Println("failed to read current battery:", err)
				continue
			}
			batterynow, err := strconv.ParseFloat(strings.TrimSpace(string(bbatterynow)), 64)
			if err != nil {
				fmt.Println("failed to parse current battery:", err)
				continue
			}
			bbatteryfull, err := os.ReadFile("/sys/class/hwmon/hwmon1/device/charge_full")
			if err != nil {
				fmt.Println("failed to read max battery:", err)
				continue
			}
			batteryfull, err := strconv.ParseFloat(strings.TrimSpace(string(bbatteryfull)), 64)
			if err != nil {
				fmt.Println("failed to parse max battery:", err)
				continue
			}
			bbatterystatus, err := os.ReadFile("/sys/class/hwmon/hwmon1/device/status")
			if err != nil {
				fmt.Println("failed to read current status of battery:", err)
				continue
			}
			batterystatus := strings.TrimSpace(string(bbatterystatus))
			batterypercentage := (batterynow * 100.0)/batteryfull
			fmt.Fprintf(self.fifo.Fifo, "bat: %0.2f%% %s\n", batterypercentage, batterystatus)
			if (batterypercentage < 10.0 &&
				batterystatus == "Discharging") {
				if ((notifyinterval % 30) == 0) {
					notifymessage := fmt.Sprintf("%0.2f%%", batterypercentage)
					beeep.Notify("low battery", notifymessage, "")
					notifyinterval = 0
				}
				notifyinterval = notifyinterval + 1
			}
		}
	}()
	return true
}

func (self *StatusBatteryPlugin) Stop() bool {
	self.stopRun = true
	return true
}

func (self *StatusBatteryPlugin) Finalize() bool {
	self.fifo.Finalize()
	return true
}
