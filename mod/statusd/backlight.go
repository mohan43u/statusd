package statusd

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

type StatusBacklightPlugin struct {
	fifo *StatusFifo
	stopRun bool
}


func (self *StatusBacklightPlugin) Initialize(args []string) bool {
	self.fifo = NewStatusFifo("backlight")
	if self.fifo == nil {
		return false
	}
	self.stopRun = false
	return true
}

func (self *StatusBacklightPlugin) Run() bool {
	go func() {
		ticker := time.Tick(time.Second)
		for range ticker {
			if self.stopRun {
				break
			}
			bbrightness, err := os.ReadFile("/sys/class/backlight/intel_backlight/brightness")
			if err != nil {
				fmt.Println("failed to read brightness:", err)
				continue
			}
			bmaxbrightness, err := os.ReadFile("/sys/class/backlight/intel_backlight/max_brightness")
			if err != nil {
				fmt.Println("failed to read maxbrightness:", err)
				continue
			}
			brightness, err := strconv.ParseFloat(strings.TrimSpace(string(bbrightness)), 64)
			if err != nil {
				fmt.Println("failed to parse brightness:", err)
				continue
			}
			maxbrightness, err := strconv.ParseFloat(strings.TrimSpace(string(bmaxbrightness)), 64)
			if err != nil {
				fmt.Println("failed to parse maxbrightness:", err)
				continue
			}
			fmt.Fprintf(self.fifo.Fifo, "scr: %0.2f%%\n", (brightness * 100.0)/maxbrightness)
		}
	}()
	return true
}

func (self *StatusBacklightPlugin) Stop() bool {
	self.stopRun = true
	return true
}

func (self *StatusBacklightPlugin) Finalize() bool {
	self.fifo.Finalize()
	return true
}
