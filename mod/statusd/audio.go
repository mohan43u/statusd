package statusd

import (
	"fmt"
	"os"
	"os/exec"
	"path"
	"strings"
)

type StatusAudioPlugin struct {
	fifo *StatusFifo
	pwclient *exec.Cmd
}

func (self *StatusAudioPlugin) Initialize(args []string) bool {
	self.fifo = NewStatusFifo("audio")
	if self.fifo == nil {
		return false
	}
	return true
}

func (self *StatusAudioPlugin) Run() bool {
	pwclientname := "pwclient"
	pwclientfilename := pwclientname
	pwclientdir := path.Dir(os.Args[0])
	if len(pwclientdir) > 0 {
		pwclientfilename = path.Clean(pwclientdir + "/../pwclient/build/" + pwclientname)
	}
	_, err := os.Lstat(pwclientfilename)
	if err != nil {
		if strings.Contains(err.Error(), "no such file or directory") {
			pwclientfilename = path.Clean(pwclientdir + "/" + pwclientname)
			_, err := os.Lstat(pwclientfilename)
			if err != nil {
				if strings.Contains(err.Error(), "no such file or directory") {
					pwclientfilename = "pwclient"
				} else {
					fmt.Println(pwclientfilename, "opening failed:", err)
				}
			}
		} else {
			fmt.Println(pwclientfilename, "opening failed:", err)
			return false
		}
	}
	self.pwclient = exec.Command(pwclientfilename)
	self.pwclient.Stdout = self.fifo.Fifo
	self.pwclient.Stderr = os.Stderr
	err = self.pwclient.Start()
	if err != nil {
		fmt.Println("failed to start pwclient:", err)
		self.pwclient = nil
		return false
	}
	go func() {
		err := self.pwclient.Wait()
		if err != nil {
			fmt.Println("pwclient not running:", err)
			self.Stop()
		}
	}()
	return true
}

func (self *StatusAudioPlugin) Stop() bool {
	if self.pwclient != nil {
		err := self.pwclient.Process.Kill()
		if err != nil {
			fmt.Println("failed to kill pwclient:", err)
		}
	}
	self.pwclient = nil
	return true
}

func (self *StatusAudioPlugin) Finalize() bool {
	self.fifo.Finalize()
	return true
}
