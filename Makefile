subdirs := mod/statusd/statusd mod/statusd/pwclient

all: $(subdirs)

$(subdirs):
	make -C $@

$(patsubst %,%.install,$(subdirs)):%.install:
	make -C $* install

install: $(patsubst %,%.install,$(subdirs))

$(patsubst %,%.clean,$(subdirs)):%.clean:
	make -C $* clean

clean: $(patsubst %,%.clean,$(subdirs))

.PHONY: $(subdirs) all install clean
